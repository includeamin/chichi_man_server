import requests


def get_assigned_package():
    a = requests.get("http://localhost:3003/package/5d8b4c5d0be67694e850fe29",
                     headers={
                         "Id": "5d87e194549ae0267b5268cc",
                         "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                     },
                     verify=False).content

    print(a)


def received_from_warehouse():
    a = requests.post("http://localhost:3003/package/state/received",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29",
                          "PackageNumber": "10"
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)


def checked_products():
    a = requests.post("http://localhost:3003/package/state/checked",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29",
                          "PackageNumber": "10"
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)


def going_to_destination():
    a = requests.post("http://localhost:3003/package/state/going",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29",
                          "PackageNumber": "10"
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)


def arrived_to_destination():
    a = requests.post("http://localhost:3003/package/state/arrived",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29",
                          "PackageNumber": "10"
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)


def delivered_package():
    a = requests.post("http://localhost:3003/package/state/delivered",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29",
                          "PackageNumber": "10",
                          "DeliveryDuration": 10.2
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)
    

def end_delivery():
    a = requests.post("http://localhost:3003/package/state/end",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29",
                          "PackageNumber": "10",
                          'Latitude': 1.2, 
                          'Longitude': 5.3, 
                          'TurnAroundTime': 20.3
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)


def submit_feed_back():
    a = requests.post("http://localhost:3003/package/feedback",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29",
                          "PackageNumber": "10",
                          'WarehouseProblems': 'warehouse problems', 
                          'CustomerProblems': 'customer problems',
                          'Rate': 5

                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)

if __name__ == "__main__":
    submit_feed_back()
