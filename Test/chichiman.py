import requests


def register_chichiman():
    a = requests.post("http://chichiapp.ir:30033/admin/chichiman/register",
                      json={
                          "PhoneNumber": "09367265647"
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                      },
                      verify=False).content

    print(a)


def get_activation_code():
    a = requests.get("http://localhost:3003/chichiman/code/09101063006",
                     headers={
                         "Id": "5d8b4c5d0be67694e850fe29",
                         "Token": "d1795f250825db9ed5f639208c8ec3e6e15d06aecee2b92e113f1d723511ec02"
                     },
                     verify=False).content

    print(a)


def activate_phone_number():
    a = requests.get("http://localhost:3003/chichiman/verify/09101063006/4301",
                     headers={
                         "Id": "5d87e194549ae0267b5268cc",
                         "Token": "6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49"
                     },
                     verify=False).content

    print(a)


def login():
    a = requests.post("http://localhost:3003/chichiman/login",
                      json={
                          "PhoneNumber": "09101063006"
                      },
                      headers={
                          "Id": "5d87e194549ae0267b5268cc",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def submit_personal_info():
    a = requests.post("http://localhost:3003/admin/chichiman/info/personal",
                      json={
                          'PhoneNumber': "09101063006",
                          'FirstName': 'امین',
                          'LastName': 'رایج',
                          'SSN': '0123456789',
                          'Serial': 'د۱۳/۱۲۳۴۵۶',
                          'ProfilePic': 'profile_pic',
                          'Birthday': '۱۴۰۰/۰۲/۰۱',
                          'DriverLicense': '0123456789',
                          'Address': 'address',
                          'MartialStatus': 'مجرد',
                          'Sex': 'مرد'
                      },
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def submit_delivery_info():
    a = requests.post("http://localhost:3003/admin/chichiman/info/delivery",
                      json={
                          'PhoneNumber': "09101063006",
                          'DeliveryType': 'موتور برقی',
                          'PlateNumber': '۶۲ل۱۲۳۴۵',
                          'CardNumber': '۱۲۱۲۱۲۱۲۱۲۳۴۵۶۷۸۹'
                      },
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def submit_contract_info():
    a = requests.post("http://localhost:3003/admin/chichiman/info/contract",
                      json={
                          'PhoneNumber': '09101063006',
                          'Image': 'image',
                          'Status': 'تعلیق',
                          'SSNCardImage': 'card_image',
                          'BasePayment': 2000,
                          'DeliveryLicenseImage': 'delivery_image',
                          'EndOfContract': 'end of contract',
                          'Percentage': '111111111111111111111111'
                      },
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def init_chichi_man_config():
    a = requests.get("http://localhost:3003/admin/chichiman/config/init",
                     headers={
                         "Id": "5d8b4c5d0be67694e850fe29",
                         "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                     },
                     verify=False).content
    print(a)


def get_chichi_man_characteristics():
    a = requests.get("http://localhost:3003/chichiman/characteristics",
                     headers={
                         "Id": "5d8b4c5d0be67694e850fe29",
                         "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                     },
                     verify=False).content

    print(a)


def init_queue():
    a = requests.post("http://localhost:3003/admin/chichiman/queue/config/init",
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def add_chichi_man_to_queue():
    a = requests.post("http://localhost:3003/system/chichiman/queue/add",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29"
                      },
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def delete_chichi_man_from_queue():
    a = requests.post("http://localhost:3003/system/chichiman/queue/delete",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29"
                      },
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def add_chichi_man_back_to_queue():
    a = requests.post("http://localhost:3003/system/chichiman/queue/addback",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29"
                      },
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def move_chichi_man_to_end_of_queue():
    a = requests.post("http://localhost:3003/system/chichiman/queue/movetoend",
                      json={
                          "ChiChiManId": "5d8b4c5d0be67694e850fe29"
                      },
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)


def get_next_available_chichi_man():
    a = requests.get("http://localhost:3003/system/chichiman/queue/nextitem",
                      headers={
                          "Id": "5d8b4c5d0be67694e850fe29",
                          "Token": "cbc1e5ce37bd7f92670b92ead41cea435cf6b952b582cb8b257f7efeb743ccc3"
                      },
                      verify=False).content

    print(a)
if __name__ == "__main__":
    register_chichiman()
