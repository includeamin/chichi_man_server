from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Authentication.Authentication import is_login, permission
from Classes.PackageManager import PackageManager
from Classes.ChiChiMan import ChiChiMan
from Classes.ChiChiManConfig import ChiChiManConfig
from Classes.Queue import Queue
from Classes.Tools import Tools
from Models.PackageManagerModel import PackageStateModel, PackageFeedBackModel

package_manager_route = APIRouter()


@package_manager_route.get('/package/{chichi_man_id}', tags=["package_manager"])
def get_assigned_package(chichi_man_id):
    try:
        return PackageManager.get_assigned_package(chichi_man_id)
    except Exception as ex:
        return Tools.Result(True, ex.args)


@package_manager_route.post('/package/state/{state}', tags=["package_manager"],
                            description=f'state can be {["received", "checked", "going", "arrived", "delivered", "end"]}')
def received_from_warehouse(request: Request, data: PackageStateModel, state):
    valid_state = ["received", "checked", "going", "arrived", "delivered", "end"]
    if state not in valid_state:
        raise HTTPException(detail=f"invalid state, states should {valid_state}", status_code=400)
    if state == 'received':
        return PackageManager.received_from_warehouse(request.headers["Id"], data.PackageNumber)
    elif state == 'checked':
        return PackageManager.checked_products(request.headers["Id"], data.PackageNumber)
    elif state == 'going':
        return PackageManager.going_to_destination(request.headers["Id"], data.PackageNumber)
    elif state == 'arrived':
        return PackageManager.arrived_to_destination(request.headers["Id"], data.PackageNumber)
    elif state == 'delivered':
        if data.DeliveryDuration is None:
            raise HTTPException(detail="DeliveryDuration is NULL", status_code=400)
        return PackageManager.delivered_package(request.headers["Id"], data.PackageNumber, data.DeliveryDuration)
    elif state == 'end':
        # todo : apply location checking
        # if data.Latitude is None or data.Longitude is None or data.TurnAroundTime is None:
        #     raise HTTPException(detail="Latitude or Longitude or is NULL TurnAroundTime", status_code=400)

        return PackageManager.end_delivery(request.headers["Id"],
                                           data.PackageNumber,
                                           data.Latitude,
                                           data.Longitude,
                                           data.TurnAroundTime)


@package_manager_route.post('/package/feedback', tags=["package_manager"])
def submit_feedback(data: PackageFeedBackModel):
    try:
        return PackageManager.submit_feedback(data.ChiChiManId,
                                              data.PackageNumber,
                                              data.WarehouseProblems,
                                              data.CustomerProblems,
                                              data.Rate
                                              )
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)


@package_manager_route.get("/package/users/history", tags=["User"])
def get_users_package_history(user_id: str, page: int = 1):
    return PackageManager.mobile_get_package_history_depends_on_state(user_id=user_id, page=page)


@package_manager_route.get("/package/users/detail", tags=["User"])
def get_users_package_detail(user_id, package_id):
    return PackageManager.mobile_get_package_info(
        user_id, package_id)


@package_manager_route.get("/system/package/auto-assigner", tags=["System"])
def system_package_auto_assigner():
    return PackageManager.system_auto_assigner()


@package_manager_route.get('/package/payment/submit', tags=["package_manager"])
def add_payment_info_in_cash(package_id: str, payment_type: str, image_id: str = None, amount_in_cash: int = None,
                             amount_in_pos: int = None
                             ):
    if payment_type == 'pos':
        pass
    elif payment_type == 'cash':
        pass
