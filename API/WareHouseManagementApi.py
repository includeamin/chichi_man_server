from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Authentication.Authentication import is_login, permission
from starlette.websockets import WebSocket
from starlette.responses import HTMLResponse
from starlette.status import WS_1008_POLICY_VIOLATION
from starlette.websockets import WebSocket
from Classes.WareHouseManagement import WareHouse

ware_house = APIRouter()


@ware_house.get("/warehouse/baskets/get/state", tags=["WareHouse"], description='get basket by states')
def ware_house_get_baskets_by_state(state: str):
    return WareHouse.ware_house_get_packages_depends_on_state(state)


@ware_house.get('/warehouse/states', tags=["WareHouse"], description='get warehouse valid states')
def ware_house_get_valid_states():
    return WareHouse.get_valid_ware_house_state()


@ware_house.get("/warehouse/baskets/detail", tags=["WareHouse"])
def warehouse_baskets_get_detail(_id):
    return WareHouse.ware_house_get_package_info(_id)


@ware_house.get("/warehouse/baskets/valid-states", tags=["WareHouse"])
def warehouse_valid_states_of_package():
    return WareHouse.get_valid_ware_house_state()


@ware_house.get("/warehouse/baskets/chichiman", tags=["WareHouse"])
def ware_house_get_baskets_chichi_man(package_id):
    return WareHouse.ware_house_get_chichi_man_of_package(package_id)
