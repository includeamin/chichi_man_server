from flask import Blueprint, request

from Classes.PackageManager import PackageManager
from Classes.Tools import Tools
from Middleware.Middlewares import json_body_required, check_form_json_key, login_required

package_manager_route = Blueprint(
    "package_manager_route", __name__, "template")


@package_manager_route.route('/package/<chichi_man_id>', methods=['GET'])
@login_required
def get_assigned_package(chichi_man_id):
    try:
        return PackageManager.get_assigned_package(chichi_man_id)
    except Exception as ex:
        return Tools.Result(True, ex.args)


@package_manager_route.route('/package/state/received', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ChiChiManId', 'PackageNumber'])
def received_from_warehouse():
    try:
        data = request.get_json()
        return PackageManager.received_from_warehouse(data['ChiChiManId'],
                                                      data['PackageNumber']
                                                      )
    except Exception as ex:
        return Tools.Result(False, ex.args)


# todo : seen mechanism in limit time


@package_manager_route.route('/package/state/checked', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ChiChiManId', 'PackageNumber'])
def checked_products():
    try:
        data = request.get_json()
        return PackageManager.checked_products(data['ChiChiManId'],
                                               data['PackageNumber']
                                               )
    except Exception as ex:
        return Tools.Result(False, ex.args)


@package_manager_route.route('/package/state/going', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ChiChiManId', 'PackageNumber'])
def going_to_destination():
    try:
        data = request.get_json()
        return PackageManager.going_to_destination(data['ChiChiManId'],
                                                   data['PackageNumber']
                                                   )
    except Exception as ex:
        return Tools.Result(False, ex.args)


@package_manager_route.route('/package/state/arrived', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ChiChiManId', 'PackageNumber'])
def arrived_to_destination():
    try:
        data = request.get_json()
        return PackageManager.arrived_to_destination(data['ChiChiManId'],
                                                     data['PackageNumber']
                                                     )
    except Exception as ex:
        return Tools.Result(False, ex.args)


@package_manager_route.route('/package/state/delivered', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ChiChiManId', 'PackageNumber', 'DeliveryDuration'])
def delivered_package():
    try:
        data = request.get_json()
        return PackageManager.delivered_package(data['ChiChiManId'],
                                                data['PackageNumber'],
                                                data['DeliveryDuration']
                                                )
    except Exception as ex:
        return Tools.Result(False, ex.args)


@package_manager_route.route('/package/state/end', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ChiChiManId', 'PackageNumber', 'Latitude', 'Longitude', 'TurnAroundTime'])
def end_delivery():
    try:
        data = request.get_json()
        return PackageManager.end_delivery(data['ChiChiManId'],
                                           data['PackageNumber'],
                                           data['Latitude'],
                                           data['Longitude'],
                                           data['TurnAroundTime']
                                           )

    except Exception as ex:
        return Tools.Result(False, ex.args)


@package_manager_route.route('/package/feedback', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ChiChiManId', 'PackageNumber', 'WarehouseProblems', 'CustomerProblems', 'Rate'])
def submit_feedback():
    try:
        data = request.get_json()
        return PackageManager.submit_feedback(data['ChiChiManId'],
                                              data['PackageNumber'],
                                              data['WarehouseProblems'],
                                              data['CustomerProblems'],
                                              data['Rate'],
                                              )
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)
