from Classes.Review import Review
from flask import Blueprint, request
from Middleware.Middlewares import json_body_required, login_required, check_form_json_key
from Classes.Tools import Tools

review_route = Blueprint("review_route", __name__, "template")


@review_route.route('/review/submit')
@json_body_required
@check_form_json_key(['ChichiManId', 'UserId', 'Review'])
def submit_review():
    try:
        # todo : check chichiman and user relation
        data = request.get_json()
        return Review.submit_review(data['ChichiManId'],
                                    data['UserId'],
                                    data['Review'])
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)
