from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Authentication.Authentication import is_login, permission
from Classes.PackageManager import PackageManager
from Classes.ChiChiMan import ChiChiMan
from Classes.ChiChiManConfig import ChiChiManConfig
from Classes.Queue import Queue
from Classes.Review import Review
from Classes.Tools import Tools
from Models.ReviewModel import ReviewModel

review_route = APIRouter()


@review_route.post('/review/submit', tags=["Review"])
def submit_review(data: ReviewModel):
    try:
        # todo : check chichiman and user relation
        return Review.submit_review(data.ChichiManId,
                                    data.UserId,
                                    data.Review)
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)
