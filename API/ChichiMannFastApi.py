from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Authentication.Authentication import is_login, permission
from Classes.PackageManager import PackageManager
from Classes.ChiChiMan import ChiChiMan
from Classes.ChiChiManConfig import ChiChiManConfig
from Classes.Queue import Queue
from Classes.Tools import Tools
from Models.ChiChiMan import UpdateInfoModel, AdminChichimanContractInfo, AdminChichimanSendSuccessMessage, \
    SystemQueueActionModel, SystemAssignChiChiMan, SystemReAssignChiChiMan, ChiChiManLoginModel, AdminChiChiManBank
from Models.ChiChiMan import AdminChichimanDeliveryInfoModel, AdminChiChiManSignUp
from fastapi import BackgroundTasks

chichi_man_route = APIRouter()


# dependencies=[Depends(is_login)]

@chichi_man_route.post('/admin/chichiman/register', tags=["ChiChiMan"])
# @permission("ChiChiManRegister")
# todo : add registerer info
async def register_chichi_man(background_tasks: BackgroundTasks, data: AdminChiChiManSignUp):
    result = await ChiChiMan.register_phone_number(background_tasks, data.PhoneNumber)
    return result


@chichi_man_route.get('/chichiman/code/{phone_number}', tags=["ChiChiMan"])
def get_activation_code(phone_number):
    try:
        return ChiChiMan.get_activation_code(phone_number)
    except Exception as ex:
        return Tools.Result(False, ex.args)


# TODO should generate token?
# TODO insert type of verification
@chichi_man_route.get('/chichiman/verify/{phonenumber}/{code}', tags=["ChiChiMan"])
def verify_phone_number(phonenumber, code):
    try:
        return ChiChiMan.verify_phone_number(phonenumber, code)
    except Exception as ex:
        return Tools.Result(False, ex.args)


# TODO login just using phone number??
@chichi_man_route.post('/chichiman/login', tags=["ChiChiMan"])
def login(data: ChiChiManLoginModel):
    try:
        return ChiChiMan.login(data.PhoneNumber)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.post('/admin/chichiman/info/personal', description="admin update chichiman's info",
                       tags=["ChiChiMan"])
def update_personal_info(data: UpdateInfoModel):
    try:

        return ChiChiMan.update_personal_info(data.PhoneNumber, data.FirstName, data.LastName, data.SSN,
                                              data.Serial, data.ProfilePic, data.Birthday,
                                              data.Address, data.MartialStatus,
                                              data.Sex, place_of_issue=data.PlaceOfIssue, home_phone=data.HomePhone,
                                              ssn_image=data.SSN_IMAGE,
                                              serial_image=data.SERIAL_IMAGE)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.post('/admin/chichiman/info/delivery', description="update delivery info of chichiman",
                       tags=["ChiChiMan"])
def update_delivery_info(data: AdminChichimanDeliveryInfoModel):
    try:
        return ChiChiMan.update_delivery_info(data.PhoneNumber, data.DeliveryType, data.PlateNumber,
                                              data.CardNumber, data.VehicleModel, data.VehicleColor,
                                              license_image=data.LicenseImage,
                                              license_number=data.LicenseNumber,
                                              vehicle_Card_mage=data.VehicleCardImage)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.post('/admin/chichiman/info/contract', description="update chichiman contract info",
                       tags=["ChiChiMan"])
@permission("ChiChiManAdmin")
def update_contract_info(data: AdminChichimanContractInfo):
    try:

        return ChiChiMan.update_contract_info(data.PhoneNumber, data.Image, data.Status,
                                              data.BasePayment, data.BeginOfContract, data.EndOfContract,
                                              data.Percentage, data.FormNumber, data.SoePishineImage,
                                              safteh=data.Safteh, attach_number=data.AttachmentNumber)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.post('/admin/chichiman/info/bank', description='update bank info of chichiman', tags=["ChiChiMan"])
@permission('ChiChiManAdmin')
def update_bank_info(data: AdminChiChiManBank):
    try:
        return ChiChiMan.update_financial(data.PhoneNumber, data.Name, data.CardNumber, data.AccountNumber,
                                          data.BankName, data.IBAN, data.BankBranch)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.post('/admin/chichiman/send/message', description="send message to chichiman", tags=["ChiChiMan"])
@permission("ChiChiManAdmin")
def send_success_message(data: AdminChichimanSendSuccessMessage):
    try:

        return ChiChiMan.send_success_message(data.PhoneNumber)
    except Exception as ex:
        return Tools.Result(False, ex.args)


#
@chichi_man_route.get('/admin/chichiman/config/init', tags=["ChiChiMan"])
@permission("ChiChiManAdmin")
def init_chichi_man_config():
    try:
        return ChiChiManConfig.init_chichi_config()
    except Exception as ex:
        return Tools.Result(False, ex.args)


# TODO solve weired response
@chichi_man_route.get('/chichiman/characteristics', tags=["ChiChiMan"])
def get_characteristics():
    try:
        return ChiChiManConfig.get_characteristics()
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.post('/admin/chichiman/queue/config/init', tags=["ChiChiMan"])
def init_queue():
    try:
        return Queue.initialize_queue()
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.post("/system/chichiman/queue/{action}", tags=["QUEUE"])
def queue(action, data: SystemQueueActionModel):
    valid_action = ['add', 'delete', 'addback', 'movetoend', 'nextitem', 'assign', 'reassign']
    if not valid_action.__contains__(action):
        raise HTTPException(detail=f"invalid action, action should be {valid_action}", status_code=400)

    if action == 'add':
        return ChiChiMan.add_to_queue(data.ChiChiManId)
    elif action == 'delete':
        return ChiChiMan.delete_from_queue(data.ChiChiManId)
    elif action == 'addback':
        return ChiChiMan.add_back_to_queue(data.ChiChiManId)
    elif action == 'movetoend':
        return ChiChiMan.move_to_end(data.ChiChiManId)
    elif action == 'nextitem':
        return ChiChiMan.next_item()


# todo :get id from header
@chichi_man_route.get("/chichiman/queue/{action}",
                      description=f'{["start_job", "back_to_store", "problem_in_delivery"]}', tags=["Queue"])
def chichiman_queue_actions(action, chichiman_id):
    valid_action = ["start_job", "back_to_store", 'problem_in_delivery']
    if action not in valid_action:
        raise HTTPException(detail=f"action should be in {valid_action}", status_code=400)
    if action == 'start_job':
        ChiChiMan.add_to_queue(chichiman_id)
    elif action == "back_to_store":
        ChiChiMan.add_back_to_queue(chichiman_id)
    elif "problem_in_delivery":
        ChiChiMan.move_to_end(chichiman_id)
    return 'd'


@chichi_man_route.get("/admin/queue/summery", tags=["Queue"], description="get summery of queue")
def admin_get_summery_of_chichiman_in_queue():
    return Queue.admin_get_queue()


@chichi_man_route.post('/system/chichiman/assign', tags=['ChiChiMan'], description="chichiman assgn")
def assign_package_to_chichi_man(data: SystemAssignChiChiMan):
    # try:
    print('called assigned')
    return PackageManager.assign_package_to_chichi_man(data.UserId,
                                                       data.Name,
                                                       data.PhoneNumber,
                                                       data.Location,
                                                       data.Receipt,
                                                       data.Products,
                                                       data.PackageNumber,
                                                       data.TotalBasketPrice,
                                                       data.DeliveryPrice,
                                                       data.BasketState,
                                                       data.StateChangingTiming
                                                       )


# except Exception as ex:
#     import traceback
#     traceback.print_exc()
#     return Tools.Result(False, ex.args)


@chichi_man_route.post('/system/chichiman/reassign', tags=["ChiChiMan"])
def reassign_package_to_chichi_man(data: SystemReAssignChiChiMan):
    try:
        return PackageManager.reassign_package_to_chichi_man(data.PackageNumber)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@chichi_man_route.get('/admin/panel/chichiman/get/{action}',
                      description='get summery of chichiman, actions = [summery,detail],if action is detail should '
                                  'set ?chichiman_id= parameter in url',
                      tags=["ChiChiMan"])
def admin_panel_chichiman_summery(action, chichiman_id=None):
    if action == 'summery':
        return ChiChiMan.admin_get_chichiman_summery()
    elif action == 'detail':
        if not chichiman_id:
            raise HTTPException(detail='chichiman_id not exist in parameters', status_code=400)
        return ChiChiMan.admin_get_chichiman_info(chichiman_id)


@chichi_man_route.get("/admin/panel/chichimans-list", tags=["ChiChiMan"])
def admin_panel_chichiman_lists(page: int = 1):
    return ChiChiMan.admin_chichimans_list(page=page)


@chichi_man_route.get("/admin/chichiman/detail", tags=["ChiChiMan"], description="get detail of chichiman")
def admin_panel_get_chichi_man_detail(_id, formatted: bool = False):
    return ChiChiMan.admin_chichiman_detail(_id, formatted)


@chichi_man_route.get("/admin/chichiman/delete", tags=["ChiChiMan"], description="delete chichiman")
def admin_delete_chichiman(_id):
    return ChiChiMan.temp_admin_delete_chichiman(_id)
