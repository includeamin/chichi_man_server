from flask import Flask, jsonify
from flask_cors import CORS
from Classes.Tools import Tools

from API.ChiChiManApi import chichi_man_route
from API.ReviewApi import review_route
from API.PackageManagerApi import package_manager_route
from Database.DB import ChiChiManDB

app = Flask(__name__)

app.register_blueprint(chichi_man_route)
app.register_blueprint(review_route)
app.register_blueprint(package_manager_route)

CORS(app)

from Classes.Queue import Queue
@app.route('/init')
def init():
    Queue.initialize_queue()
    return Tools.Result(True, 'd')


@app.route("/")
def what():
    return jsonify({"What": "chichiman",
                    "Author": "AminJamal",
                    "NickName": "Includeamin",
                    "Email": "aminjamal10@gmail.com",
                    "WebSite": "includeamin.com"})

@app.get('/healthz')
def check_liveness():
    inv = ChiChiManDB.find_one({}, {})
    return 'd'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3003, debug=True)
