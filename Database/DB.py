import pymongo
import json
import os
import requests
from Classes.Tools import Tools

Decode = Tools.decode
Initer = Tools.initer

dirpath = os.getcwd()

DEBUG = False
print("amin")
if DEBUG is not True:
    with open(os.path.join(dirpath, "Database/DatabaseConfigs.json")) as f:
        configs = json.load(f)
    print("amin2")
    get_database_url = json.loads(
        requests.get("{0}/Services/config/get/ChiChiManManagement".format(configs['MicroServiceManagementURl']),
                    ).content)
    print("amin3")

    if not get_database_url["State"]:
        exit(1)

    get_database_url = json.loads(get_database_url["Description"])
    print(get_database_url)

    url = ''
    if get_database_url["Key"] is None:
        url = Decode(get_database_url["DatabaseString"])
    else:
        Initer(get_database_url["Key"])
        url = Decode(get_database_url["DatabaseString"])

else:
    url = 'localhost:27017'
    configs = {
        'DatabaseName': 'ChiChiManManagement',
        'ChiChiManCollection': 'ChiChiMan',
        'ChiChiManConfig': 'ChiChiManConfig',
        'Queue': 'Queue',
        'Review': 'Review',
        'PackageManager': 'PackageManager'
    }

mongodb = pymongo.MongoClient(url)
database = mongodb[configs["DatabaseName"]]
ChiChiManDB = database[configs["ChiChiManCollection"]]
ChiChiManConfigDB = database[configs['ChiChiManConfig']]
Queue = database[configs['Queue']]
review_collection = database[configs['Review']]
package_manager_collection = database[configs['PackageManager']]
