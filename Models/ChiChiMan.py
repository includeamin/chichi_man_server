from pydantic import BaseModel


class UpdateInfoModel(BaseModel):
    PhoneNumber: str
    FirstName: str
    LastName: str
    SSN: str
    Serial: str
    ProfilePic: str
    Birthday: str
    Address: str
    MartialStatus: str
    Sex: str
    PlaceOfIssue: str = None
    HomePhone: str = None
    SSN_IMAGE: str
    SERIAL_IMAGE: str


class AdminChichimanDeliveryInfoModel(BaseModel):
    PhoneNumber: str
    DeliveryType: str
    PlateNumber: str
    CardNumber: str
    VehicleModel: str = 'None'
    VehicleColor: str = 'blue'
    VehicleCardImage: str = None
    LicenseNumber: str = None
    LicenseImage: str = None


class AdminChichimanContractInfo(BaseModel):
    PhoneNumber: str
    Image: str
    Status: str
    BasePayment: str
    EndOfContract: str
    BeginOfContract: str
    Percentage: str
    FormNumber: str
    AttachmentNumber: str
    SoePishineImage: str
    Safteh: str


class AdminChiChiManBank(BaseModel):
    PhoneNumber: str
    Name: str
    CardNumber: str
    AccountNumber: str
    BankName: str
    IBAN: str
    BankBranch: str


class AdminChichimanSendSuccessMessage(BaseModel):
    PhoneNumber: str


class SystemQueueActionModel(BaseModel):
    ChiChiManId: str = None


class SystemAssignChiChiMan(BaseModel):
    UserId: str
    Name: str
    PhoneNumber: str
    Location: dict
    Receipt: str
    Products: list
    PackageNumber: str
    TotalBasketPrice: str
    DeliveryPrice: str
    BasketState: str
    StateChangingTiming: dict


class SystemReAssignChiChiMan(BaseModel):
    PackageNumber: str


class ChiChiManLoginModel(BaseModel):
    PhoneNumber: str


class AdminChiChiManSignUp(BaseModel):
    PhoneNumber: str


class AdminSummeryOfChiChiManModel(BaseModel):
    ChiChiMans: list


class AdminChiChiManInfoModel(BaseModel):
    ChiChiMan: dict


AdminChiChiManDetailResponse = {
    "Identify": {
        "header": "اهراز هویت",
        "sub": {
            "شماره موبایل": "",

            "کد 4 رقمی اهراز هویت": ""
        }

    },
    "PersonalInfo": {
        "header": "اطلاعات شخصی",
        "sub": {
            "نام و نام خانوادگی": "",
            "شماره تماس منزل": "",
            "شماره شناسنامه": "",
            "شماره ملی": "",
            "آدرس": ""
        },
        "Images": {
            "Profile": "",
            "SERIAL_IMAGE": "",
            "SSN_IMAGE": "SSN_IMAGE"
        }

    },
    "vehicle": {
        "header": "اطلاعات وسیله نقلیه",
        "sub": {
            "شماره گواهینامه": "",
            "نام وسیله نقلیه": "",
            "شماره پلاک": "",
            "شماره کارت وسیله نقیله": ""
        },
        "images":
            {
                "LicenseImage": "",
                "VehicleCardImage": ""
            }

    },
    "contract": {
        "header": "مستندات قرارداد",
        "sub": {
            "تاریخ شروع": "",
            "تاریخ پایان": "",
            "شماره نامه/فرم": "",
            "شماره پیوست": "",
            "حقوق ثابت": "",
            "قرارداد درصدی": "",
            "وضعیت قرارداد": ""
        },
        "images": {
            "contract": "",
            "soePishine": "",
            "safteh": ""
        }
    },
    "BankInfo": {
        "header": "اطلاعات بانکی",
        "sub": {
            "شماره کارت": "",
            "شماره حساب": "",
            "نام بانک": "",
            "شعبه": "",
            "شماره شبا": "",
            "نام و نام خاتوادگی صاحب حساب": ""
        }

    },

}
