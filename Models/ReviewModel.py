from pydantic import BaseModel


class ReviewModel(BaseModel):
    ChichiManId: str
    UserId: str
    Review: str
