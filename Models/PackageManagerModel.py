from pydantic import BaseModel


class PackageStateModel(BaseModel):
    PackageNumber: str
    Latitude: str = None
    Longitude: str = None
    TurnAroundTime: str = None
    DeliveryDuration: str = None


class PackageFeedBackModel(BaseModel):
    ChiChiManId: str
    PackageNumber: str
    WarehouseProblems: str
    CustomerProblems: str
    Rate: str
