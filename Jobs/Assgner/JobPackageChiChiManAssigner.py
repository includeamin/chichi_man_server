import requests


def check_availability_of_chichiman():
    result = requests.get("http://chichiapp.ir:30033/system/job/chichiman/availability")
    if result.status_code == 200:
        return result.content
    else:
        return False


def check_availability_of_unassigned_package():
    result = requests.get("http://chichiapp.ir:30033/system/job/packagemanager/last-package-not-assigned")
    if result.status_code == 200:
        return result.content
    else:
        return False


"""
assigning chichiman to package and sent notification
"""


def assigner():
    pass
