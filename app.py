from fastapi import FastAPI, HTTPException
from starlette.responses import HTMLResponse, JSONResponse
from starlette.middleware.cors import CORSMiddleware
from uvicorn import run
from API.ChichiMannFastApi import chichi_man_route
from API.PackageManagerFastApi import package_manager_route
from API.ReviewFastApi import review_route
from Database.DB import ChiChiManDB
from API.WareHouseManagementApi import ware_house
from Database.DB import ChiChiManDB

app = FastAPI()

app.include_router(chichi_man_route)
app.include_router(package_manager_route)
app.include_router(review_route)
app.include_router(ware_house)

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(HTTPException)
async def http_exception(request, exc):
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)


@app.get("/", tags=["Main"])
def index():
    return {"Email": "aminjamaml10@gmail.com", "Name": "amin jamal", "Nickname": "includeamin"}


@app.get('/healthz')
def check_liveness():
    inv = ChiChiManDB.find_one({}, {})
    return 'd'


if __name__ == '__main__':
    run(app, host='0.0.0.0', port=3003, debug=True)
