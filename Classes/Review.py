from datetime import datetime

from Classes.Tools import Tools
from Classes.ChiChiMan import ChiChiMan
from Database.DB import review_collection


class Review:

    def __init__(self, chichi_man_id, user_id, review):
        self.ChichiManId = chichi_man_id
        self.UserId = user_id
        self.Review = review
        self.Created_at = datetime.now()

    @staticmethod
    def submit_review(chichi_man_id, user_id, rate):
        # submit review
        review_collection.insert_one(Review(
            chichi_man_id,
            user_id,
            rate
        ).__dict__)

        # update chichi man total rate
        ChiChiMan.update_rate(chichi_man_id, rate)

        return Tools.Result(True, 'd')
