from bson import ObjectId
from fastapi import HTTPException
from Classes.PackageManager import PackageManager
from Classes.ChiChiMan import ChiChiMan
from Database.DB import package_manager_collection
from Classes.Tools import Tools


class WareHouse:
    ware_house_valid_state = {
        "WaitForAssign": PackageManager.Basket_states["WaitForAssign"],
        "Assigned": PackageManager.Basket_states["Assigned"],
        "InProgress": "InProgress"
    }

    @staticmethod
    def calculate_gathering_timing():
        return 204

    @staticmethod
    def ware_house_get_packages_depends_on_state(state=None):

        if state not in WareHouse.ware_house_valid_state:
            raise HTTPException(detail=f"invalid state {WareHouse.ware_house_valid_state}", status_code=400)

        if state == WareHouse.ware_house_valid_state["InProgress"]:
            condition = {
                '$and': [{"PackageState": {'$ne': "Assigned"}}, {"PackageState": {'$ne': "WaitForAssign"}}]}


        else:
            condition = {
                "PackageState": state
            }
        projection = {
            "PackageNumber",
            f"StateChangingTiming",
            "PackageState",
            "ChichiManId"
        }

        data = package_manager_collection.find(condition, projection)
        res = []
        for item in data:
            item["_id"] = str(item["_id"])
            if "ChichiManId" in item and item['ChichiManId']:
                item["ChichiManId"] = str(item["ChichiManId"])
            if not item["ChichiManId"]:
                item.pop("ChichiManId")
            item["StateChangingTiming"] = item["StateChangingTiming"][item["PackageState"]]
            res.append(item)
        return res

    @staticmethod
    def ware_house_get_chichi_man_of_package(package_id):
        package = package_manager_collection.find_one({"_id": ObjectId(package_id)}, {"PackageState", "ChichiManId"})
        if not package:
            raise HTTPException(detail="package not found", status_code=400)
        if package["PackageState"] == PackageManager.Basket_states["WaitForAssign"]:
            raise HTTPException(detail="package not assigned", status_code=400)
        chichi_man_id = package['ChichiManId']
        return ChiChiMan.ware_house_get_chichi_man_info(chichi_man_id)

    @staticmethod
    def get_valid_ware_house_state():
        return list(WareHouse.ware_house_valid_state.keys())

    @staticmethod
    def ware_house_get_package_info(basket_id):
        package = package_manager_collection.find_one({"_id": ObjectId(basket_id)},
                                                      {"Products", "_id", "UserInfo.Receipt", "StateChangingTiming",
                                                       "PackageState"})
        package["_id"] = str(package["_id"])
        package["StateChangingTiming"] = package["StateChangingTiming"][package["PackageState"]]
        if not package:
            raise HTTPException(detail="basket not exist", status_code=400)
        for item in package["Products"]:
            item["Images"] = Tools.url_maker(item["Images"])

        return package
