from datetime import datetime

from Classes.Tools import Tools
from Database.DB import Queue as queue_collection
from bson import ObjectId
from fastapi import HTTPException


class Queue:

    def __init__(self, item=None):
        self.Items = [
            {
                'Item': item,
                'Timestamp': Queue.get_current_timestamp(),
                'State': 'NotAvailable'
            }
        ] if item is not None else []

    @staticmethod
    def get_current_timestamp():
        return int(datetime.now().timestamp())

    @staticmethod
    def initialize_queue():
        queue_collection.insert_one(Queue().__dict__)
        return Tools.Result(True, 'd')

    @staticmethod
    def add_to_queue(item):
        # make sure id is not duplicated
        is_duplicated = queue_collection.find_one({'Items.Item._id': item['_id']}) is not None

        if is_duplicated:
            return Tools.Result(False, Tools.errors('IAE'))

        # insert item into queue
        queue_collection.update_one({}, {
            '$push': {
                'Items': {
                    'Item': item,
                    'State': 'Available',
                    'Timestamp': Queue.get_current_timestamp()
                }
            }
        })

        return Tools.Result(True, 'd')

    @staticmethod
    def delete_from_queue(item):

        # make sure item does exist in the queue
        in_queue = queue_collection.find_one({'Items.Item._id': item['_id']}) is not None

        if not in_queue:
            return Tools.Result(False, Tools.errors('INF'))

        # change state of the item to not available
        queue_collection.update_one(
            {},
            {
                '$set': {'{}.$[elem].{}'.format('Items', 'State'): 'NotAvailable'}
            },
            array_filters=[{'elem.Item._id': item['_id']}]
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def add_back_to_queue(item):
        # make sure item does exist in the queue
        in_queue = queue_collection.find_one({'Items.Item._id': ObjectId(item['_id'])}) is not None

        if not in_queue:
            return Tools.Result(False, Tools.errors('INF'))

        # change state of the item to not available
        queue_collection.update_one(
            {},
            {
                '$set': {'{}.$[elem].{}'.format('Items', 'State'): 'Available'}
            },
            array_filters=[{'elem.Item._id': ObjectId(item['_id'])}]
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def move_to_end(item):
        # make sure item does exist in the queue
        in_queue = queue_collection.find_one({'Items.Item._id': ObjectId(item['_id'])}) is not None

        if not in_queue:
            return Tools.Result(False, Tools.errors('INF'))

        # change state of the item to not available
        queue_collection.update_one(
            {},
            {
                '$set': {'{}.$[elem].{}'.format('Items', 'Timestamp'): Queue.get_current_timestamp()}
            },
            array_filters=[{'elem.Item._id': ObjectId(item['_id'])}]
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def make_item_busy(item):
        # make sure item does exist in the queue
        in_queue = queue_collection.find_one({'Items.Item._id': ObjectId(item['_id'])}) is not None

        if not in_queue:
            return Tools.Result(False, Tools.errors('INF'))

        # change state of the item to not available
        queue_collection.update_one(
            {},
            {
                '$set': {'{}.$[elem].{}'.format('Items', 'State'): 'Busy'}
            },
            array_filters=[{'elem.Item._id': ObjectId(item['_id'])}]
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def next_item():

        pipeline = [
            {'$unwind': '$Items'},
            {'$match': {'Items.State': 'Available'}},
            {'$sort': {'Items.Timestamp': -1}}
        ]
        items = list(queue_collection.aggregate(pipeline))

        if len(items) > 0:
            return items[0]['Items']['Item']
        else:
            return None

    @staticmethod
    def system_job_check_is_available_chichi_exist():
        pipeline = [
            {'$unwind': '$Items'},
            {'$match': {'Items.State': 'Available'}},
            {'$sort': {'Items.Timestamp': -1}}
        ]
        items = list(queue_collection.aggregate(pipeline))
        if len(items) > 0:
            return items[0]['Items']['Item']
        # raise
        return None

    @staticmethod
    def admin_get_queue():
        data = queue_collection.find_one({})
        data["_id"] = str(data["_id"])
        for item in data["Items"]:
            item["Item"]["_id"] = str(item["Item"]["_id"])
            item["Item"]["ProfilePic"] = Tools.url_maker(item["Item"]["ProfilePic"])
        return data
