import json
import random
import re
from datetime import datetime
from fastapi import BackgroundTasks

import requests
from bson import ObjectId

from Classes import Bridge
from Classes.Queue import Queue
from Classes.Tools import Tools
from Database.DB import ChiChiManDB as chichi_collection, ChiChiManConfigDB as chichi_config_collection
from Models.ChiChiMan import AdminSummeryOfChiChiManModel, AdminChiChiManInfoModel, AdminChiChiManDetailResponse
from fastapi import HTTPException
from Bridges.Bridge import send_code_phone_number


class ChiChiMan:
    queue_projection = {
        "First_Name", "Last_Name", "_id", "PhoneNumber", "ProfilePic"
    }

    def __init__(self, phone_number, code):
        self.ContractInfo = {
            "Image": None,
            "Create_at": None,
            "Status": None,
            "BasePayment": None,
            "EndOfContract": None,
            "Percentage": None
        }
        self.Status = {
            'Text': None,
            'Description': None
        }
        self.Code = {
            'Code': code,
            'Is_Used': False
        }
        self.PersonalInfo = {
            "PhoneNumber": phone_number,
            'First_Name': None,
            'Last_Name': None,
            'SSN': None,
            'Serial': None,
            'ProfilePic': None,
            'Birthday': None,
            'Address': None,
            'MartialStatus': None,
            'Sex': None,
            'PlaceOfIssue': None,
            'HomePhone': None,
            "SSN_IMAGE": None,
            "SERIAL_IMAGE": None
        }
        self.DeliveryInfo = {
            'DeliveryType': None,
            'PlateNumber': None,
            'CardNumber': None,
            'VehicleModel': None,
            'VehicleColor': 'blue',
            'VehicleCardImage': None,
            "LicenseImage": None,
            "DriverLicense": None
        }

        # personal
        # self.First_Name = None
        # self.Last_Name = None
        # self.PhoneNumber = phone_number
        # self.SSN = None
        # self.Serial = None
        # self.ProfilePic = None
        # self.Birthday = None
        # self.DriverLicense = None
        # self.Address = None
        # self.MartialStatus = None
        # self.Sex = None
        #
        # self.WorkExperience = None
        # self.FatherName = None
        # self.PlaceOfIssue = None
        # self.HomePhone = None

        # delivery
        # self.DeliveryType = None
        # self.PlateNumber = None
        #
        # self.CardNumber = None
        # self.VehicleModel = None
        # self.VehicleColor = None
        # self.DeliveryStatus = None
        self.Financial = None
        # self.Delivery_History = [{
        #     'Created_at': None,
        #     'Delivery_at': None,
        #     'PackageNumber': None,
        #     'Destination': {
        #         'Lat': None,
        #         'Lang': None,
        #         'Text_Address': None
        #     },
        #     'DeliveryDuration': None,
        #     'TurnAroundDuration': None,
        #     'System_Recommended_Time': None,
        #     'Total_Cost': None,
        #     "Delivery_Income": None,
        #     "Distance": None,
        #     'Feedback': {
        #         'WarehouseProblems': None,
        #         'CustomerProblems': None,
        #         'Rate': None
        #     },
        #     "Customer_Info": {
        #         "Name": None,
        #         "PhoneNumber": None
        #     }
        # }],
        self.Financial_Info = {
            "Total": None,
            "PaymentRecords": [{
                "Create_at": None,
                "Amount": None,
                "Description": None,
                "Type": None
            }]
        }
        self.States = {
            "TotalRate": 0,
            "TotalWeight": 0,
            "Total_Distance": 0
        }

        self.Create_at = datetime.now()
        self.Update_at = datetime.now()

        # states
        self.Is_Phone_Number_Entered = True
        self.Is_Phone_Number_Verified = False
        self.Is_Personal_Info_Entered = False
        self.Is_Delivery_Info_Entered = False
        self.Is_Contract_Info_Entered = False
        self.Is_Finance_Info_Entered = False

    class Constants:
        regex = {
            'alphabet': r'[ﺍ ﺁ ﺏ ﭖ ﺕ ﺙ ﺝ ﭺ ﺡ ﺥ ﺩ ﺫ ﺭ ﺯ ﺱ ﺵ ﺹ ﺽ ﻁ ﻅ ﻉ ﻍ ﻑ ﻕ ﮎ ﮒ ﻝ ﻡ ﻥ ﻭ ﻩ ﯼ]',
            'phone_regex': r'[0-9]{11}$',
            'activation_code_regex': r'[0-9]{4}',
            'ssn_regex': r'[0-9]{10}$',
            'name_regex': r'[ا آ ب پ ت ث ج چ ح خ د ذ ر ز س ش ص ض ط ظ ع غ ف ق ک گ ل م ن و ه ی]{3,}',
            'serial_regex': r'[ا آ ب پ ت ث ج چ ح خ د ذ ر ز س ش ص ض ط ظ ع غ ف ق ک گ ل م ن و ه ی][۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹]{2}/[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹]{6}$',
            'birthday_regex': r'[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹]{4}/[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹]{2}/[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹]{2}$',
            'driver_license_regex': r'[0-9]{10}$',
            'martial_status_regex': r'(متاهل|مجرد)$',
            'state_code_regex': r'[0-9]{3}$',
            'home_phone_regex': r'[0-9]{7}$',
            'delivery_type_regex': r'(موتور برقی|موتور بنزینی|خودرو|دوچرخه)$',
            'plate_number_regex': r'[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹]{2}[ا آ ب پ ت ث ج چ ح خ د ذ ر ز س ش ص ض ط ظ ع غ ف ق ک گ ل م ن و ه ی][۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹]{5}$',
            'card_number_regex': r'[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ا آ ب پ ت ث ج چ ح خ د ذ ر ز س ش ص ض ط ظ ع غ ف ق ک گ ل م ن و ه ی]{17}$',
            'contract_info_account_number_regex': r'[0-9]{24}$',
            'contract_info_status_regex': r'(اتمام|اخراج|تعلیق|فعال|)$',
            'sex_regex': r'(مرد|زن)$'
        }

    @staticmethod
    def validate_pattern(pattern, regex):
        return re.match(ChiChiMan.Constants.regex[regex], pattern)

    @staticmethod
    def generate_activation_code():
        return random.randint(1000, 9999)

    @staticmethod
    def ware_house_get_chichi_man_info(chichi_man_id):
        chichi_man = chichi_collection.find_one({"_id": ObjectId(chichi_man_id)}, {"PersonalInfo.ProfilePic",
                                                                                   "PersonalInfo.PhoneNumber",
                                                                                   "PersonalInfo.First_Name",
                                                                                   "PersonalInfo.Last_Name",
                                                                                   "DeliveryInfo.VehicleModel",
                                                                                   "DeliveryInfo.PlateNumber"
                                                                                   })
        if not chichi_man:
            raise HTTPException(detail="not found", status_code=400)
        chichi_man["_id"] = str(chichi_man["_id"])
        chichi_man["PersonalInfo"]["ProfilePic"] = Tools.url_maker(chichi_man["PersonalInfo"]["ProfilePic"])
        return chichi_man

    @staticmethod
    async def register_phone_number(background_tasks: BackgroundTasks, phone_number):
        # validate phone number
        if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None:
            return Tools.Result(False, Tools.errors('NA'))

        # make sure phone number is unique
        is_duplicated = chichi_collection.find_one(
            {'PersonalInfo.PhoneNumber': phone_number}, {'_id': 1}) is not None

        if is_duplicated:
            return Tools.Result(False, Tools.errors('IAE'))

        # generate activation code
        activation_code = ChiChiMan.generate_activation_code()

        # send activation code
        # send_result = \
        # todo : apply this after debug time
        # send_code_phone_number(background_tasks, phone_number, activation_code)

        # parse result
        # parsed_result = json.loads(send_result)
        #
        # if parsed_result['State'] is False:
        #     return Tools.Result(False, Tools.errors('INF'))

        # store chichi man info
        chichi_collection.insert_one(
            ChiChiMan(phone_number, activation_code).__dict__)

        return Tools.Result(True, 'd')

    @staticmethod
    def get_activation_code(phone_number):
        activation_code = chichi_collection.find_one(
            {'PersonalInfo.PhoneNumber': phone_number}, {'_id': 0, 'Code.Code': 1})
        print(activation_code)

        if activation_code is None:
            return Tools.Result(False, Tools.errors('INF'))

        return Tools.Result(True, activation_code)

    @staticmethod
    def verify_phone_number(phone_number, activation_code):

        # validate phone number and activation code
        if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None or \
                ChiChiMan.validate_pattern(str(activation_code), 'activation_code_regex') is None:
            return Tools.Result(False, Tools.errors('NA'))

        # make sure phone number is registered and code is not used
        chichi_object = chichi_collection.find_one(
            {
                'PersonalInfo.PhoneNumber': phone_number,
                'Code.Code': int(activation_code),
                'Code.Is_Used': False,
                'Is_Phone_Number_Entered': True
            },
            {'_id': 1})

        if chichi_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        # validate the phone number
        chichi_collection.update_one(
            {'PersonalInfo.PhoneNumber': phone_number},
            {
                '$set': {
                    'Code.Is_Used': True,
                    'Is_Phone_Number_Verified': True,
                    'Update_at': datetime.now()
                }
            }
        )

        token = Bridge.gen_token_authentication(
            user_id=str(chichi_object["_id"]))

        if not token:
            return Tools.Result(False, Tools.errors("FTGT"))

        response = {
            'Id': chichi_object["_id"],
            'Token': token
        }

        return Tools.Result(True, Tools.dumps(response))

    @staticmethod
    def update_personal_info(phone_number, first_name, last_name, ssn, serial, profile_pic, birthday,
                             address, martial_status, sex, work_experience=None, father_name=None, place_of_issue=None,
                             home_phone=None, ssn_image=None, serial_image=None):

        # make sure phone number is valid
        # if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None or \
        #         ChiChiMan.validate_pattern(first_name, 'name_regex') is None or \
        #         ChiChiMan.validate_pattern(last_name, 'name_regex') is None or \
        #         ChiChiMan.validate_pattern(ssn, 'ssn_regex') is None or \
        #         ChiChiMan.validate_pattern(serial, 'serial_regex') is None or \
        #         ChiChiMan.validate_pattern(driver_license, 'driver_license_regex') is None or \
        #         ChiChiMan.validate_pattern(martial_status, 'martial_status_regex') is None or \
        #         ChiChiMan.validate_pattern(sex, 'sex_regex') is None or \
        #         (father_name is not None and ChiChiMan.validate_pattern(father_name, 'name_regex') is None) is None or \
        #         (home_phone is not None and ChiChiMan.validate_pattern(home_phone, 'home_phone_regex') is None):
        #     return Tools.Result(False, Tools.errors('NA'))

        # make sure user id verified
        is_verified = chichi_collection.find_one(
            {
                'PersonalInfo.PhoneNumber': phone_number,
                'Code.Is_Used': True,
                'Is_Phone_Number_Verified': True
            },
            {'_id': 1}) is not None

        if not is_verified:
            return Tools.Result(False, Tools.errors('NA'))

        # update information
        chichi_collection.update_one(
            {'PersonalInfo.PhoneNumber': phone_number},
            {
                '$set': {
                    'PersonalInfo.First_Name': first_name,
                    'PersonalInfo.Last_Name': last_name,
                    'PersonalInfo.SSN': ssn,
                    'PersonalInfo.Serial': serial,
                    'PersonalInfo.ProfilePic': profile_pic,
                    'PersonalInfo.Birthday': birthday,
                    'PersonalInfo.Address': address,
                    'PersonalInfo.MartialStatus': martial_status,
                    'PersonalInfo.Sex': sex,
                    'PersonalInfo.PlaceOfIssue': place_of_issue,
                    'PersonalInfo.HomePhone': home_phone,
                    'Is_Personal_Info_Entered': True,
                    'Update_at': datetime.now(),
                    "PersonalInfo.SSN_IMAGE": ssn_image,
                    "PersonalInfo.SERIAL_IMAGE": serial_image
                }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def update_delivery_info(phone_number, delivery_type, plate_number=None, card_number=None, vehicle_model=None,
                             vehicle_color=None, license_number=None, license_image=None, vehicle_Card_mage=None):

        # # make sure phone number is valid
        # if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None or \
        #         ChiChiMan.validate_pattern(delivery_type, 'delivery_type_regex') is None or \
        #         (plate_number is not None and ChiChiMan.validate_pattern(plate_number,
        #                                                                  'plate_number_regex') is None) or \
        #         (card_number is not None and ChiChiMan.validate_pattern(card_number, 'card_number_regex') is None):
        #     return Tools.Result(False, Tools.errors('NA'))

        # make sure user id verified
        is_verified = chichi_collection.find_one(
            {
                'PersonalInfo.PhoneNumber': phone_number,
                'Code.Is_Used': True,
                'Is_Personal_Info_Entered': True
            },
            {'_id': 1}) is not None

        if not is_verified:
            return Tools.Result(False, Tools.errors('NA'))

        # make sure plate number is not registered before
        is_duplicate = chichi_collection.find_one(
            {'PlateNumber': plate_number}, {'_id': 1}) is not None

        if is_duplicate:
            return Tools.Result(False, Tools.errors('IAE'))

        # update delivery info
        chichi_collection.update_one(
            {'PersonalInfo.PhoneNumber': phone_number},
            {
                '$set': {
                    'DeliveryInfo.DeliveryType': delivery_type,
                    'DeliveryInfo.PlateNumber': plate_number,
                    'DeliveryInfo.CardNumber': card_number,
                    'DeliveryInfo.VehicleModel': vehicle_model,
                    'DeliveryInfo.VehicleColor': 'blue',
                    'DeliveryInfo.VehicleCardImage': vehicle_Card_mage,
                    "DeliveryInfo.LicenseImage": license_image,
                    "DeliveryInfo.DriverLicense": license_number,
                    'Is_Delivery_Info_Entered': True,
                    'Update_at': datetime.now()
                }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def update_contract_info(phone_number, image, status, base_payment, begin_of_contract,
                             end_of_contract, percentage, form_number, soe_pishine, safteh, attach_number):
        # make sure phone number is valid
        # if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None or \
        #         ChiChiMan.validate_pattern(status, 'contract_info_status_regex') is None or \
        #         ChiChiMan.validate_pattern(account_number, 'contract_info_account_number_regex') is None:
        #     return Tools.Result(False, Tools.errors('NA'))

        # make sure user id verified
        is_verified = chichi_collection.find_one(
            {
                'PersonalInfo.PhoneNumber': phone_number,
                'Code.Is_Used': True,
                'Is_Delivery_Info_Entered': True
            },
            {'_id': 1}) is not None

        if not is_verified:
            return Tools.Result(False, Tools.errors('NA'))

        # update contract info
        chichi_collection.update_one(
            {'PersonalInfo.PhoneNumber': phone_number},
            {
                '$set': {
                    'ContractInfo.Image': image,
                    'ContractInfo.Create_at': datetime.now(),
                    'ContractInfo.Status': status,
                    'ContractInfo.BasePayment': base_payment,
                    'ContractInfo.BeginOfContract': begin_of_contract,
                    'ContractInfo.EndOfContract': end_of_contract,
                    'ContractInfo.Percentage': percentage,
                    'ContractInfo.FormNumber': form_number,
                    'ContractInfo.SoePishine': soe_pishine,
                    'ContractInfo.Safteh': safteh,
                    'Is_Contract_Info_Entered': True,
                    'Update_at': datetime.now(),
                    "ContractInfo.AttachNumber": attach_number
                }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def fill_check_state(phone_number):
        exist = chichi_collection.find_one({"PhoneNumber": phone_number}, {
            "Is_Contract_Info_Entered",
            "Is_Phone_Number_Verified",
            "Is_Personal_Info_Entered",
            "Is_Delivery_Info_Entered",
            "Is_Contract_Info_Entered"
        })

        if not exist:
            raise Exception('chichi man not found')

        if not exist["Is_Contract_Info_Entered"]:
            pass
        if not exist["Is_Delivery_Info_Entered"]:
            pass
        if not exist["Is_Personal_Info_Entered"]:
            pass
        if not exist["Is_Contract_Info_Entered"]:
            pass
        if not exist["Is_Finance_Info_Entered"]:
            pass

    @staticmethod
    def update_financial(phone_number, name, card_number, account_number, bank_name, iban, bank_branch):

        exist = chichi_collection.find_one({"PersonalInfo.PhoneNumber": phone_number}, {
            "Is_Contract_Info_Entered",
            "Is_Phone_Number_Verified",
            "Is_Personal_Info_Entered",
            "Is_Delivery_Info_Entered",
            "Is_Contract_Info_Entered",
            "Is_Finance_Info_Entered",
            "Is_Phone_Number_Entered"
        })
        if not exist:
            raise Exception('chichi man not found')
        if not exist['Is_Phone_Number_Entered']:
            raise Exception('phone number not entred')
        if not exist["Is_Phone_Number_Verified"]:
            raise Exception('phone number not verified')
        if not exist["Is_Personal_Info_Entered"]:
            raise Exception('personal info not entred')
        if not exist["Is_Delivery_Info_Entered"]:
            raise Exception('delivery info not entred')
        if not exist["Is_Contract_Info_Entered"]:
            raise Exception('contract info not entred')

        result = chichi_collection.update_one({"_id": exist["_id"]}, {
            "$set": {
                "Financial": {
                    "Name": name,
                    "CardNumber": card_number,
                    "AccountNumber": account_number,
                    "BankName": bank_name,
                    "IBAN": iban,
                    "BankBranch": bank_branch
                },
                'Update_at': datetime.now(),
                "Is_Finance_Info_Entered": True
            }
        })
        if result.matched_count == 0:
            raise Exception("NothingsChanged")

        return Tools.Result(True, 'd')

    @staticmethod
    def send_success_message(phone_number):

        # make sure phone number is valid
        if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None:
            return Tools.Result(False, Tools.errors('NA'))

        # make sure user id verified
        is_verified = chichi_collection.find_one(
            {
                'PersonalInfo.PhoneNumber': phone_number,
                'Code.Is_Used': True,
                'Is_Contract_Info_Entered': True
            },
            {'_id': 1}) is not None

        if not is_verified:
            return Tools.Result(False, Tools.errors('NA'))

        # get success message from config collection
        success_message = chichi_config_collection.find_one(
            {}, {'SuccessMessage': 1})['SuccessMessage']

        result = Bridge.send_activation_sms(phone_number, success_message)

        parsed_result = json.loads(result)

        if parsed_result['State'] is False:
            return Tools.Result(False, parsed_result['Description'])
        else:
            return Tools.Result(True, 'd')

    @staticmethod
    def update_rate(chichi_man_id, rate):

        # find the chichi man
        chichi_man_object = chichi_collection.find_one(
            {'_id': ObjectId(chichi_man_id)}, {'TotalRate': 1, 'TotalWeight': 1})

        if chichi_man_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        total_rate = chichi_man_object['TotalRate']
        total_weight = chichi_man_object['TotalWeight']

        average = (rate + total_rate) / (total_weight + 1)

        # update chichi man rating
        chichi_collection.update_one(
            {'_id': ObjectId(chichi_man_id)},
            {
                '$set': {
                    'TotalRate': average,
                    'TotalWeight': total_weight + 1
                }
            }
        )

    @staticmethod
    def add_to_queue(chichi_man_id):
        # todo : apply projection

        # find chichi man
        chichi_man_object = chichi_collection.find_one(
            {'_id': ObjectId(chichi_man_id)}, ChiChiMan.queue_projection)

        if chichi_man_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        Queue.add_to_queue(chichi_man_object)

        return Tools.Result(True, 'd')

    @staticmethod
    def delete_from_queue(chichi_man_id):

        # find chichi man
        chichi_man_object = chichi_collection.find_one(
            {'_id': ObjectId(chichi_man_id)})

        if chichi_man_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        Queue.delete_from_queue(chichi_man_object)

        return Tools.Result(True, 'd')

    @staticmethod
    def add_back_to_queue(chichi_man_id):

        # find chichi man
        chichi_man_object = chichi_collection.find_one(
            {'_id': ObjectId(chichi_man_id)}, ChiChiMan.queue_projection)

        if chichi_man_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        Queue.add_back_to_queue(chichi_man_object)

        return Tools.Result(True, 'd')

    @staticmethod
    def move_to_end(chichi_man_id):

        # find chichi man
        chichi_man_object = chichi_collection.find_one(
            {'_id': ObjectId(chichi_man_id)}, ChiChiMan.queue_projection)

        if chichi_man_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        Queue.move_to_end(chichi_man_object)

        return Tools.Result(True, 'd')

    @staticmethod
    def next_item():

        next_chichi_man = Queue.next_item()

        return Tools.Result(True, Tools.dumps(next_chichi_man))

    @staticmethod
    def login(phone_number):

        # validate phone number
        if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None:
            return Tools.Result(False, Tools.errors('NA'))

        # make sure user with specified phone number is registered and confirmed
        chichi_man_object = chichi_collection.find_one({
            'PersonalInfo.PhoneNumber': phone_number
        }, {
            '_id': 1
        })

        if chichi_man_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        # generate activation code
        activation_code = ChiChiMan.generate_activation_code()

        # send activation code to phone number
        activation_result = Bridge.send_activation_sms(
            phone_number, activation_code)

        # # parse result
        # result_dict = json.loads(activation_result)
        #
        # # check whether sending sms was successful
        # successful = result_dict['State']
        #
        # # if it failed -> forward the result
        # if not successful:
        #     return activation_result

        chichi_collection.update_one(
            {'PersonalInfo.PhoneNumber': phone_number},
            {
                '$set': {
                    'Code.Code': activation_code,
                    'Code.Is_Used': False
                }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def login_verification(phone_number, verification_code):
        # validate phone number and activation code
        if ChiChiMan.validate_pattern(phone_number, 'phone_regex') is None or \
                ChiChiMan.validate_pattern(str(verification_code), 'activation_code_regex') is None:
            return Tools.Result(False, Tools.errors('NA'))

        # check whether user is registered and is assigned the specified activation code
        # also make sure code is not used before
        user_object = chichi_collection.find_one({
            'PersonalInfo.PhoneNumber': phone_number,
            'Code.Code': int(verification_code),
            'Code.Is_Used': False
        }, {
            '_id': 1
        })

        if user_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        chichi_collection.update_one(
            {'PersonalInfo.PhoneNumber': phone_number},
            {
                '$set': {
                    'Update_at': datetime.now(),
                    'Code.Is_Used': True
                }
            }
        )

        token = Bridge.gen_token_authentication(
            user_id=str(user_object["_id"]))

        if not token:
            return Tools.Result(False, Tools.errors("FTGT"))

        response = {
            'Id': user_object["_id"],
            'Token': token
        }

        return Tools.Result(True, Tools.dumps(response))

    @staticmethod
    def push_delivery_history(chichi_man_id, assigned_at, delivery_at, package_number,
                              dest_lat, dest_long, dest_address, delivery_duration, turn_around_duration,
                              system_recommended_time, total_cost, distance, warehouse_problems, customer_problems,
                              rate,
                              customer_name, customer_phonenumber):

        valid = chichi_collection.find_one(
            {'_id': ObjectId(chichi_man_id)}, {'_id': 1}) is not None

        if not valid:
            return Tools.Result(False, Tools.errors('INF'))

        chichi_collection.update_one(
            {'_id': ObjectId(chichi_man_id)},
            {
                '$push': {
                    'Delivery_History': {
                        'Created_at': assigned_at,
                        'Delivery_at': delivery_at,
                        'PackageNumber': package_number,
                        'Destination': {
                            'Lat': dest_lat,
                            'Lang': dest_long,
                            'Text_Address': dest_address
                        },
                        'DeliveryDuration': delivery_duration,
                        'TurnAroundDuration': turn_around_duration,
                        'System_Recommended_Time': system_recommended_time,
                        'Total_Cost': total_cost,
                        "Delivery_Income": None,
                        "Distance": distance,
                        'Feedback': {
                            'WarehouseProblems': warehouse_problems,
                            'CustomerProblems': customer_problems,
                            'Rate': rate
                        },
                        "Customer_Info": {
                            "Name": customer_name,
                            "PhoneNumber": customer_phonenumber
                        }
                    }
                }
            }
        )

    @staticmethod
    def admin_get_chichiman_summery():
        items = chichi_collection.find({}, {'First_Name', 'Last_Name', 'PhoneNumber', 'ProfilePic', 'DeliveryType'})
        res = []
        for item in items:
            item["_id"] = str(item["_id"])
            item["ProfilePic"] = Tools.url_maker(item["ProfilePic"])
            res.append(item)

        return AdminSummeryOfChiChiManModel(ChiChiMans=res)

    @staticmethod
    def admin_get_chichiman_info(_id):
        item = chichi_collection.find_one({"_id": ObjectId(_id)})
        if not item:
            raise HTTPException(detail='chichi man with this id not found', status_code=400)
        item['_id'] = str(item['_id'])
        item["ProfilePic"] = Tools.url_maker(item["ProfilePic"])
        return AdminChiChiManInfoModel(ChiChiMan=item)

    @staticmethod
    def admin_chichimans_list(page=1):
        skip, page = Tools.pagination(page=page)
        data = chichi_collection.find({}, {
            "PersonalInfo", "DeliveryInfo"
        }).skip(
            skip).limit(page)
        res = []
        for item in data:
            res.append({
                "_id": str(item["_id"]),
                "name": f'{item["PersonalInfo"]["First_Name"]} {item["PersonalInfo"]["Last_Name"]}',
                'phoneNumber': item["PersonalInfo"]["PhoneNumber"],
                "vehicle": item["DeliveryInfo"]["DeliveryType"],
                "image": Tools.url_maker(item["PersonalInfo"]['ProfilePic']),
                "Plaque": item["DeliveryInfo"]["PlateNumber"]
            })

        return Tools.status_code_result(200, res)

    @staticmethod
    def admin_chichiman_detail(chichi_man_id, formatted: bool = False):
        chichiman = chichi_collection.find_one({"_id": ObjectId(chichi_man_id)})
        # chichi_collection.update_many({}, {"$set": {"ContractInfo.AttachNumber": 123}})
        if not chichiman:
            raise HTTPException(detail="item not found", status_code=400)
        chichiman["_id"] = str(chichiman["_id"])
        if not formatted:
            chichiman['PersonalInfo']["ProfilePic"] = Tools.url_maker(chichiman['PersonalInfo']["ProfilePic"])
            chichiman['PersonalInfo']["SERIAL_IMAGE"] = Tools.url_maker(
                chichiman['PersonalInfo']["SERIAL_IMAGE"])
            chichiman['PersonalInfo']["SSN_IMAGE"] = Tools.url_maker(chichiman['PersonalInfo']["SSN_IMAGE"])
            chichiman['DeliveryInfo']["LicenseImage"] = Tools.url_maker(chichiman['DeliveryInfo']["LicenseImage"])
            chichiman['DeliveryInfo']["VehicleCardImage"] = Tools.url_maker(
                chichiman['DeliveryInfo']["VehicleCardImage"])
            chichiman["ContractInfo"]["Image"] = Tools.url_maker(chichiman["ContractInfo"]["Image"])
            chichiman["ContractInfo"]["SoePishine"] = Tools.url_maker(chichiman["ContractInfo"]["SoePishine"])
            chichiman["ContractInfo"]["Safteh"] = Tools.url_maker(chichiman["ContractInfo"]["Safteh"])

            return chichiman
        response = AdminChiChiManDetailResponse
        response["Identify"]["sub"]["شماره موبایل"] = chichiman["PersonalInfo"]["PhoneNumber"]
        response["Identify"]["sub"]["کد 4 رقمی اهراز هویت"] = chichiman["Code"]["Code"]
        response["PersonalInfo"]["sub"][
            "نام و نام خانوادگی"] = f"{chichiman['PersonalInfo']['First_Name']} {chichiman['PersonalInfo']['Last_Name']}"
        response["PersonalInfo"]["sub"]["شماره تماس منزل"] = chichiman['PersonalInfo']["HomePhone"]
        response["PersonalInfo"]["sub"]["شماره شناسنامه"] = chichiman['PersonalInfo']["Serial"]
        response["PersonalInfo"]["sub"]["شماره ملی"] = chichiman['PersonalInfo']["SSN"]
        response["PersonalInfo"]["sub"]["آدرس"] = chichiman['PersonalInfo']["Address"]
        response["PersonalInfo"]["Images"]["Profile"] = Tools.url_maker(chichiman['PersonalInfo']["ProfilePic"])
        response["PersonalInfo"]["Images"]["SERIAL_IMAGE"] = Tools.url_maker(chichiman['PersonalInfo']["SERIAL_IMAGE"])
        response["PersonalInfo"]["Images"]["SSN_IMAGE"] = Tools.url_maker(chichiman['PersonalInfo']["SSN_IMAGE"])
        response["vehicle"]["sub"]["شماره گواهینامه"] = chichiman['DeliveryInfo']["DriverLicense"]
        response["vehicle"]["sub"]["نام وسیله نقلیه"] = chichiman['DeliveryInfo']["DeliveryType"]
        response["vehicle"]["sub"]["شماره پلاک"] = chichiman['DeliveryInfo']["PlateNumber"]
        response["vehicle"]["sub"]["شماره کارت وسیله نقیله"] = chichiman['DeliveryInfo']["CardNumber"]
        response["vehicle"]["images"]["LicenseImage"] = Tools.url_maker(chichiman['DeliveryInfo']["LicenseImage"])
        response["vehicle"]["images"]["VehicleCardImage"] = Tools.url_maker(
            chichiman['DeliveryInfo']["VehicleCardImage"])
        response["contract"]["sub"]["تاریخ شروع"] = chichiman["ContractInfo"]["BeginOfContract"]
        response["contract"]["sub"]["تاریخ پایان"] = chichiman["ContractInfo"]["EndOfContract"]
        response["contract"]["sub"]["شماره نامه/فرم"] = chichiman["ContractInfo"]["FormNumber"]
        response["contract"]["sub"]["شماره پیوست"] = chichiman["ContractInfo"]["AttachNumber"]
        response["contract"]["sub"]["حقوق ثابت"] = chichiman["ContractInfo"]["BasePayment"]
        response["contract"]["sub"]["قرارداد درصدی"] = chichiman["ContractInfo"]["Percentage"]
        response["contract"]["sub"]["وضعیت قرارداد"] = chichiman["ContractInfo"]["Status"]
        response["contract"]["images"]["contract"] = Tools.url_maker(chichiman["ContractInfo"]["Image"])
        response["contract"]["images"]["soePishine"] = Tools.url_maker(chichiman["ContractInfo"]["SoePishine"])
        response["contract"]["images"]["safteh"] = Tools.url_maker(chichiman["ContractInfo"]["Safteh"])
        response["BankInfo"]["sub"]["شماره کارت"] = chichiman["Financial"]["CardNumber"]
        response["BankInfo"]["sub"]["شماره حساب"] = chichiman["Financial"]["AccountNumber"]
        response["BankInfo"]["sub"]["نام بانک"] = chichiman["Financial"]["BankName"]
        response["BankInfo"]["sub"]["شعبه"] = chichiman["Financial"]["BankBranch"]
        response["BankInfo"]["sub"]["شماره شبا"] = chichiman["Financial"]["IBAN"]
        response["BankInfo"]["sub"]["نام و نام خاتوادگی صاحب حساب"] = chichiman["Financial"]["Name"]

        return response

    @staticmethod
    def chichiman_get_history_of_delivery(chichiman_id):
        chichiman = chichi_collection.find_one({"_id": ObjectId(chichiman_id)})
        if not chichiman:
            raise HTTPException(detail="chichiman not found", status_code=400)

    @staticmethod
    def temp_admin_delete_chichiman(_id):
        chichi_collection.delete_one({'_id': ObjectId(_id)})
        return 'd'
