from Classes.Queue import Queue
from Classes.Tools import Tools
from Database.DB import package_manager_collection
from datetime import datetime
from Classes.ChiChiMan import ChiChiMan
from bson import ObjectId
from fastapi import HTTPException


class PackageManager:
    Basket_states = {
        "Current": "Current",
        "UserInformationDeliveryConfirm": "UserInformationDeliveryConfirm",
        "ConfirmPaymentInfo": "ConfirmPaymentInfo",
        "WaitForWareHouse": "WaitForWareHouse",
        "Gathering": "Gathering",
        "WaitForAssign": "WaitForAssign",
        "Assigned": "Assigned",
        "ReceivedFromWareHouse": "ReceivedFromWareHouse",
        "Checked": "Checked",
        "Delivering": "Delivering",
        "WaitForCustomer": "WaitForCustomer",
        "DeliveredToCustomer": "DeliveredToCustomer",
        "ReturnToWareHouse": "ReturnToWareHouse",
        "Survey": "Survey",
        "NormalCancelled‌": "NormalCancelled‌",
        "CancelledWithPenalty": "CancelledWithPenalty",
        "ChiChiManProblem": "ChiChiManProblem"

    }

    def __init__(self, user_id, name, phone_number, location, receipt, products, chichi_man_id,
                 estimated_time, distance, package_number, total_basket_price, delivery_price,
                 basket_state, states_timing):
        # basket related data
        self.Products = [*products]

        self.UserInfo = {
            'UserId': user_id,
            'Name': name,
            'PhoneNumber': phone_number,
            'Location': location,
            'Receipt': receipt
        }

        # chichi man related data
        self.ChichiManId = chichi_man_id

        # estimated time
        self.EstimatedTime = estimated_time

        # user related data
        self.PackageNumber = package_number

        self.TotalBasketPrice = total_basket_price,
        self.DeliveryPrice = delivery_price

        self.Feedback = {
            'WarehouseProblems': None,
            'CustomerProblems': None,
            'Rate': None
        }
        self.PackageState = basket_state
        self.StateChangingTiming = states_timing
        self.Distance = distance
        self.Create_at = datetime.now()

    @staticmethod
    def assign_package_to_chichi_man(user_id, name, phone_number, location, receipt, products,
                                     package_number, total_basket_price, delivery_price, basket_state, states_timing):
        # todo : check if someone available or not
        # see who is at head of the queue
        chichi_man = Queue.next_item()

        is_assigned = False

        if not chichi_man:
            basket_state = PackageManager.Basket_states["WaitForAssign"]
            chichi_man_id = None
            # raise HTTPException(detail="no chichiman available", status_code=400)
        else:
            # move him to the end of queue
            is_assigned = True
            Queue.make_item_busy(chichi_man)
            basket_state = PackageManager.Basket_states["Assigned"]
            states_timing[PackageManager.Basket_states["Assigned"]] = datetime.now().timestamp()

            # assigned_at = datetime.now()
            chichi_man_id = chichi_man['_id']
        central_shop_geo = {
            'Lat': 12.2,
            'Long': 13.2
        }
        # TODO: calculate estimated time
        estimated_time = PackageManager.calculate_estimated_time(
            12.2, 13.2, location['Geo']['Lat'], location['Geo']['Lang']
        )

        # TODO: calculate distance
        distance = (central_shop_geo['Lat'] - location['Geo']['Lat']) ** 2 + \
                   (central_shop_geo['Long'] - location['Geo']['Lang']) ** 2

        # assign chichi man to specified package
        package_manager_collection.insert_one(PackageManager(
            user_id,
            name,
            phone_number,
            location,
            receipt,
            products,
            chichi_man_id,
            estimated_time,
            distance,
            package_number,
            total_basket_price,
            delivery_price, basket_state=basket_state,
            states_timing=states_timing
        ).__dict__)
        if is_assigned:
            return Tools.Result(True, 'assigned')
        return Tools.Result(True, 'not-assign')

    @staticmethod
    def get_assigned_package(chichi_man_id):
        # todo : apply projection
        assigned_package = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), "PackageState": "Assigned"})

        if assigned_package is None:
            return Tools.Result(False, Tools.errors('INF'))
        assigned_package["_id"] = str(assigned_package["_id"])
        assigned_package["ChichiManId"] = str(assigned_package["ChichiManId"])
        return Tools.Result(True, assigned_package)

    @staticmethod
    def received_from_warehouse(chichi_man_id, package_number):

        valid = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), 'PackageNumber': package_number}, {'_id': 1}) is not None

        if not valid:
            return Tools.Result(False, 'INF')

        package_manager_collection.update_one(
            {'ChichiManId': ObjectId(chichi_man_id),
             'PackageNumber': package_number},
            {
                '$set': {'PackageState': PackageManager.Basket_states["ReceivedFromWareHouse"],
                         f'StateChangingTiming.{PackageManager.Basket_states["ReceivedFromWareHouse"]}':
                             datetime.now().timestamp()
                         }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def checked_products(chichi_man_id, package_number):

        valid = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), 'PackageNumber': package_number,
             'PackageState': PackageManager.Basket_states["ReceivedFromWareHouse"]}, {'_id': 1}) is not None

        if not valid:
            return Tools.Result(False, 'INF')

        package_manager_collection.update_one(
            {'ChichiManId': ObjectId(chichi_man_id),
             'PackageNumber': package_number},
            {
                '$set': {'PackageState': PackageManager.Basket_states["Checked"],
                         f'StateChangingTiming.{PackageManager.Basket_states["Checked"]}':
                             datetime.now().timestamp()
                         }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def going_to_destination(chichi_man_id, package_number):

        valid = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), 'PackageNumber': package_number,
             "PackageState": PackageManager.Basket_states["Checked"]},
            {'_id': 1}) is not None

        if not valid:
            return Tools.Result(False, 'INF')

        package_manager_collection.update_one(
            {'ChichiManId': ObjectId(chichi_man_id),
             'PackageNumber': package_number},
            {
                '$set': {'PackageState': PackageManager.Basket_states["Delivering"],
                         f'StateChangingTiming.{PackageManager.Basket_states["Delivering"]}':
                             datetime.now().timestamp()
                         }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def arrived_to_destination(chichi_man_id, package_number):

        valid = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), 'PackageNumber': package_number,
             'PackageState': PackageManager.Basket_states["Delivering"]},
            {'_id': 1}) is not None

        if not valid:
            return Tools.Result(False, 'INF')

        package_manager_collection.update_one(
            {'ChichiManId': ObjectId(chichi_man_id),
             'PackageNumber': package_number},
            {
                '$set':
                    {'PackageState': PackageManager.Basket_states["WaitForCustomer"],
                     f'StateChangingTiming.{PackageManager.Basket_states["WaitForCustomer"]}':
                         datetime.now().timestamp()
                     }
            }
        )

        # TODO: push notification to user that delivery arrived

        return Tools.Result(True, 'd')

    """
    if delivery payment is cash, should set information of cash payment 
    """

    @staticmethod
    def delivered_package(chichi_man_id, package_number, delivery_duration, payment_with: str = None,
                          cash_amount: int = None, pos_amount: int = None, remain_to_balance: str = None):

        # todo : check users confirm packages or not

        valid = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), 'PackageNumber': package_number,
             "PackageState": PackageManager.Basket_states["WaitForCustomer"]
             }, {'_id': 1}) is not None

        if not valid:
            return Tools.Result(False, 'INF')

        if payment_with:
            pass

        package_manager_collection.update_one(
            {'ChichiManId': ObjectId(chichi_man_id),
             'PackageNumber': package_number},
            {
                '$set': {'PackageState': PackageManager.Basket_states["DeliveredToCustomer"],
                         f'StateChangingTiming.{PackageManager.Basket_states["DeliveredToCustomer"]}':
                             datetime.now().timestamp()
                         }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def end_delivery(chichi_man_id, package_number, latitude, longitude, turn_around_time):

        valid = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), 'PackageNumber': package_number,
             'PackageState': PackageManager.Basket_states["DeliveredToCustomer"]
             },
            {'_id': 1}) is not None

        if not valid:
            return Tools.Result(False, 'INF')

        # TODO: make sure chichiman is near warehouse
        # TODO: make a bridge call to get center shop geo

        package_manager_collection.update_one(
            {'ChichiManId': ObjectId(chichi_man_id),
             'PackageNumber': package_number},
            {
                '$set': {'PackageState': PackageManager.Basket_states["ReturnToWareHouse"],
                         f'StateChangingTiming.{PackageManager.Basket_states["ReturnToWareHouse"]}':
                             datetime.now().timestamp()
                         }
            }
        )
        ChiChiMan.add_back_to_queue(chichi_man_id)

        # TODO: submit delivery info in chichi man collection

        return Tools.Result(True, 'd')

    @staticmethod
    def calculate_estimated_time(source_lat, source_long, dest_lat, dest_long):
        # TODO: implement this
        return (source_lat - dest_lat) ** 2 + (source_long - dest_long) ** 2

    @staticmethod
    def reassign_package_to_chichi_man(package_number):
        # see who is at head of the queue
        chichi_man = Queue.next_item()

        if not chichi_man:
            raise HTTPException(detail='not available chichiman', status_code=400)

        # move him to the end of queue
        Queue.make_item_busy(chichi_man)

        # assign chichi man to specified package
        package_manager_collection.update_one(
            {'PackageNumber': package_number},
            {
                '$set': {'ChichiManId': chichi_man['_id'],
                         "PackageState": PackageManager.Basket_states["Assigned"],
                         f'StateChangingTiming.{PackageManager.Basket_states["Assigned"]}': datetime.now().timestamp()
                         }
            }
        )

        return Tools.Result(True, 'd')

    @staticmethod
    def submit_feedback(chichi_man_id, package_number, warehouse_problems, customer_problems, rate):

        package_object = package_manager_collection.find_one(
            {'ChichiManId': ObjectId(chichi_man_id), 'PackageNumber': int(package_number),
             'PackageState': PackageManager.Basket_states["ReturnToWareHouse"]})

        if package_object is None:
            return Tools.Result(False, 'INF')

        package_manager_collection.update_one(
            {'ChichiManId': ObjectId(chichi_man_id),
             'PackageNumber': int(package_number)},
            {
                '$set': {
                    'WarehouseProblems': warehouse_problems,
                    'CustomerProblems': customer_problems,
                    'Rate': rate,
                    'PackageState': PackageManager.Basket_states["Survey"],
                    f'StateChangingTiming.{PackageManager.Basket_states["Survey"]}': datetime.now().timestamp()
                }
            }
        )
        # todo : change chichiman history mechanism
        # push delivery history
        # ChiChiMan.push_delivery_history(chichi_man_id,
        #                                 package_object['AssignedAt'],
        #                                 package_object['DeliveryAt'],
        #                                 package_object['PackageNumber'],
        #                                 package_object['UserInfo']['Location']['Geo']['Lat'],
        #                                 package_object['UserInfo']['Location']['Geo']['Lang'],
        #                                 package_object['UserInfo']['Location']['Address']['City'] + " " +
        #                                 package_object['UserInfo']['Location']['Address']['Street'] + " " +
        #                                 package_object['UserInfo']['Location']['Address']['PostalCode'],
        #                                 package_object['DeliveryDuration'],
        #                                 package_object['TurnAroundDuration'],
        #                                 package_object['EstimatedTime'],
        #                                 package_object['TotalBasketPrice'] + package_object['TotalBasketPrice'],
        #                                 package_object['Distance'],
        #                                 package_object['Feedback']['WarehouseProblems'],
        #                                 package_object['Feedback']['CustomerProblems'],
        #                                 package_object['Feedback']['Rate'],
        #                                 package_object['UserInfo']['Name'],
        #                                 package_object['UserInfo']['PhoneNumber']
        #                                 )

        # move chichi man back to queue
        Queue.add_back_to_queue({'_id': chichi_man_id})
        Queue.move_to_end({'_id': chichi_man_id})

        return Tools.Result(True, 'd')

    @staticmethod
    def package_status(data):
        if data["ReceivedFromWarehouse"]:
            return "ReceivedFromWarehouse"
        elif data["CheckedProducts"]:
            return "CheckedProducts"
        elif data["GoingToDestination"]:
            return "GoingToDestination"
        elif data["ArrivedToDestination"]:
            return "ArrivedToDestination"
        elif data["DeliveredPackage"]:
            return "DeliveredPackage"
        elif data["EndDelivery"]:
            return "EndDelivery"

    @staticmethod
    def mobile_get_package_history_depends_on_state(user_id, page=1):
        skip, page = Tools.pagination(page=page)
        history = package_manager_collection.find({"UserInfo.UserId": user_id}, {"UserInfo.Name",
                                                                                 "UserInfo.Receipt",
                                                                                 "UserInfo.UserId",
                                                                                 "TotalBasketPrice",
                                                                                 "PackageState",
                                                                                 "StateChangingTiming",
                                                                                 "PackageNumber"
                                                                                 }).skip(skip).limit(page)
        res = []
        for item in history:
            item["_id"] = str(item["_id"])
            res.append(item)

        return Tools.status_code_result(200, res)

    @staticmethod
    def mobile_get_package_info(user_id, package_id):
        package = package_manager_collection.find_one({"UserInfo.UserId": user_id,
                                                       "_id": ObjectId(package_id)}, {
                                                          "Products",
                                                          "UserInfo",
                                                          "EstimatedTime",
                                                          "PackageNumber",
                                                          "TotalBasketPrice",
                                                          "DeliveryPrice",
                                                          "PackageState",
                                                          "StateChangingTiming",
                                                          "PackageNumber"

                                                      })
        if not package:
            raise HTTPException(status_code=400, detail="item not found")

        package["_id"] = str(package["_id"])
        for item in package["Products"]:
            item["Images"] = Tools.url_maker(item["Images"][0])
        return Tools.status_code_result(200, package)

    @staticmethod
    def get_top_ranked_package_that_not_assigned():
        pipeline = [
            {'$unwind': '$StateChangingTiming'},
            {'$match': {'ChichiManId': None}},
            {'$sort': {'StateChangingTiming.WaitForAssign': -1}}
        ]
        items = list(package_manager_collection.aggregate(pipeline))
        if len(items) > 0:
            return items[0]
        return None

    @staticmethod
    def system_auto_assign(chichiman_id, package_id):
        state = PackageManager.Basket_states["Assigned"]
        Queue.make_item_busy({"_id": chichiman_id})
        result = package_manager_collection.update_one({
            "_id": package_id
        },
            {"$set": {
                "PackageState": state,
                f"StateChangingTiming.{state}": datetime.now().timestamp(),
                "ChichiManId": chichiman_id

            }})
        if result.modified_count <= 0:
            raise HTTPException(detail='nothings has been updated', status_code=400)

    @staticmethod
    def system_auto_assigner():
        count_of_assigned = 0
        while True:
            none_assign_chichiman = Queue.system_job_check_is_available_chichi_exist()
            if none_assign_chichiman:
                none_assigned_package = PackageManager.get_top_ranked_package_that_not_assigned()
                print("nine assugned chichi man")
                if none_assigned_package:
                    PackageManager.system_auto_assign(none_assign_chichiman["_id"], none_assigned_package["_id"])
                    count_of_assigned += 1
                else:
                    break
            else:
                break
        return {
            "CountOfAssigned": count_of_assigned
        }

    @staticmethod
    def get_history_of_chichiman(chichiman_id):
        data = package_manager_collection.find_one({"ChichiManId": chichiman_id})
        for item in data:
            pass

    @staticmethod
    def chichiman_report_problem_for_delivering(chichiman_id: str, is_in_ware_house: bool, problem_description: str):
        pass
