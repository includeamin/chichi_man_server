from Database.DB import ChiChiManConfigDB as chichi_config_collection
from Classes.Tools import Tools


class ChiChiManConfig:

    def __init__(self, success_message, positive_characteristic, negative_characteristic):
        self.SuccessMessage = success_message
        self.PositiveCharacteristic = positive_characteristic
        self.NegativeCharacteristic = negative_characteristic

    @staticmethod
    def init_chichi_config():
        success_message = 'عملیات با موفقیت انجام شد'
        positive_characteristic = ['سروقت بودن']
        negative_characteristic = ['ظاهر نامناسب']
        chichi_config_collection.insert_one(
            ChiChiManConfig(success_message,
                            positive_characteristic,
                            negative_characteristic).__dict__
        )

        return Tools.Result(True, 'd')
    @staticmethod
    def get_characteristics():
        config_object = chichi_config_collection.find_one({},
                                                          {'PositiveCharacteristic': 1, 'NegativeCharacteristic': 1})

        characteristics = {
            'Positive': config_object['PositiveCharacteristic'],
            'Negative': config_object['NegativeCharacteristic'],
        }

        return Tools.Result(True, Tools.dumps(characteristics))
